// package: fcp.message_mobile.v1
// file: v1/message_mobile/message.proto

import * as v1_message_mobile_message_pb from "../../v1/message_mobile/message_pb";
import {grpc} from "@improbable-eng/grpc-web";

type ChatServiceGetMessages = {
  readonly methodName: string;
  readonly service: typeof ChatService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_message_mobile_message_pb.GetMessagesRequest;
  readonly responseType: typeof v1_message_mobile_message_pb.GetMessagesResponse;
};

export class ChatService {
  static readonly serviceName: string;
  static readonly GetMessages: ChatServiceGetMessages;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class ChatServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getMessages(
    requestMessage: v1_message_mobile_message_pb.GetMessagesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_message_mobile_message_pb.GetMessagesResponse|null) => void
  ): UnaryResponse;
  getMessages(
    requestMessage: v1_message_mobile_message_pb.GetMessagesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_message_mobile_message_pb.GetMessagesResponse|null) => void
  ): UnaryResponse;
}

